/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scannerdepuertos;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Jorge Loayza
 */
public class ScannerDePuertos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnknownHostException {
        // TODO code application logic here
        String host = "localhost";
        InetAddress inetAddress = InetAddress.getByName(host);

        String hostName = inetAddress.getHostName();
        
        for (int port = 0; port <= 65535; port++) {
            
            try {
                Socket socket = new Socket(hostName, port);
                
                String string = "Hay un servidor en el puerto " + port;
                
                System.out.println(string);
                
                socket.close();
                
            } catch (IOException e) {
            }
        }
    }
    
}
